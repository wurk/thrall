﻿using System.Globalization;
using JetBrains.Annotations;
using Wurk.Base;
using Wurk.Base.Options;
using Wurk.Dado.Extensions.Clr;
using Wurk.Dado.Extensions.String;

namespace Wurk.Thrall.Options {
    public struct ServiceOptions : IOptions {

        public string Name { get; set; }

        public string Description { get; set; }

        public string SystemName { get; set; }

        public bool RestartAutomatically { get; set; }

        public short MaximumFailures { get; set; }

        public ServiceAccountType AccountType { get; set; }

        public string CustomAccount { get; set; }

        /// <summary>
        ///     Defaults this instance.
        /// </summary>
        /// <returns>
        /// </returns>
        [UsedImplicitly]
        public static ServiceOptions Default =>
            new ServiceOptions {
                Name = Runtime.Clr.EntryAssembly.GetTitle(),
                SystemName = Runtime.Clr.EntryAssembly.GetTitle().ToShortName(),
                Description = Runtime.Clr.EntryAssembly.GetDescription(),
                AccountType = ServiceAccountType.LocalSystem,
                MaximumFailures = 3,
                RestartAutomatically = true
            };


        private static string SystemSafeName( string name )
            => CultureInfo.CurrentCulture.TextInfo.ToTitleCase( name ).Replace( @" ", string.Empty );
    }
}