﻿namespace Wurk.Thrall.Options {
    /// <summary>
    /// Represents the account type required to run the system
    /// </summary>
    public enum ServiceAccountType {
        LocalService,
        LocalSystem,
        NetworkService,
        Custom
    }
}