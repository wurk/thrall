﻿using System;
using Serilog;
using Serilog.Core;
using System.Threading.Tasks;
using Serilog.Events;

/// <summary>
/// Used by the ModuleInit. All code inside the Initialize method is ran as soon as the assembly is loaded.
/// </summary>
public static class ModuleInitializer {
    internal static Logger Logger { get; } = CreateLogger();

    /// <summary>
    /// Initializes the module.
    /// </summary>
    public static void Initialize() {

        Log.Logger = Logger;

        TaskScheduler.UnobservedTaskException += (sender, a) =>{
            Log.Logger.Error( a.Exception, "Unobserved Task Exception" );
            a.SetObserved();
        };
    }

    private static Logger CreateLogger() =>
        new LoggerConfiguration().ReadFrom.AppSettings()
                                 .Enrich.WithProcessId()
                                 .Enrich.WithMachineName()
                                 .Enrich.WithEnvironmentUserName()
                                 .Filter.ByExcluding( ExcludeLogs )
                                 .CreateLogger();

    private static bool ExcludeLogs( LogEvent arg ) =>
        arg.MessageTemplate.Text.Contains( "Configuration Result:" )
        || arg.MessageTemplate.Text.Contains( "{0} v{1}" );

}