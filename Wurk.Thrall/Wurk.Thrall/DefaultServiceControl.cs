﻿using System;
using Anotar.Serilog;
using Topshelf;

namespace Wurk.Thrall {
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="Topshelf.ServiceControl" />
    public class DefaultServiceControl<T> : ServiceControl where T : Thrall<T>, IDisposable, new() {

        public bool Start( HostControl hostControl ) {
            HostController = hostControl;
            try {
                Thrall<T>.StartInstanceService();
                ServiceStarted?.Invoke( hostControl, EventArgs.Empty );
            } catch ( Exception e ) {
                LogTo.Error( e, "Service start failed" );
                throw;
            }
            return true;
        }

        private static HostControl HostController { get; set; }

        public bool Stop( HostControl hostControl ) {
            try {
                Thrall<T>.StopInstanceService();
                ServiceStopped?.Invoke( hostControl, EventArgs.Empty );

                ServiceStarted = null;
                ServiceStopped = null;
            } catch ( Exception e ) {
                LogTo.Error( e, "Service shutdown failed" );
                throw;
            }
            return true;
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public static void Stop() => HostController.Stop();

        /// <summary>
        /// Restarts this instance.
        /// </summary>
        public static void Restart() {
            HostController.Stop();
            Thrall<T>.Initialise();
        }

        /// <summary>
        /// Occurs when the service has been started
        /// </summary>
        public static event EventHandler ServiceStarted;
        /// <summary>
        /// Occurs when the service has been stopped.
        /// </summary>
        public static event EventHandler ServiceStopped;
    }
}