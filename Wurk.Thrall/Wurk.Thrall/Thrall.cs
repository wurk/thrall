﻿using System;
using Anotar.Serilog;
using JetBrains.Annotations;
using Topshelf;
using Topshelf.HostConfigurators;
using Wurk.Base;
using Wurk.Base.Values;
using Wurk.Dado.Guard;
using Wurk.Thrall.Options;

namespace Wurk.Thrall {
    /// <summary>
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    /// <seealso cref="T:Wurk.Thrall.DefaultServiceControl`1" />
    public abstract class Thrall<TService> : ServiceController<DefaultServiceControl<TService>>
        where TService : Thrall<TService>, IDisposable, new() {


        private static TService Instance { get; set; }

        private static bool _initialised;
        private static bool _stopped;

        private static ServiceOptions Options => Options<ServiceOptions>.Current;

        private static void ConfigureRuntime( HostConfigurator hostConfigurator ) {
            
            Ensure.Instantiated( hostConfigurator, nameof(hostConfigurator) );

            // Our default service controller, pointing at a `TService` instance
            hostConfigurator.Service<DefaultServiceControl<TService>>();

            // default to our configured Serilog sinks
            hostConfigurator.UseSerilog();

           
            hostConfigurator.EnableShutdown();

            hostConfigurator.StartAutomatically();
            hostConfigurator.OnException( exception => LogTo.Error( "TopShelf threw an exception", exception ) );


            // Check for extant `ServiceOptions` by default
            try {
                if ( (Runtime.Mode != RuntimeMode.Test) && !Options<ServiceOptions>.Exists() )
                    Options<ServiceOptions>.Reset();
                ConfigureCustomRuntime( hostConfigurator );

            } catch ( Exception exception ) {
                LogTo.Error( "Unable to load/use default service configuration {exception}", exception );
            }

            ApplyCommandLine( hostConfigurator );

        }

        private static void ApplyCommandLine( HostConfigurator runtime ) {
            switch ( Runtime.Mode ) {
                case RuntimeMode.Development:
                case RuntimeMode.Diagnostics:
                case RuntimeMode.Instrumented:
                case RuntimeMode.Live:
                    runtime.ApplyCommandLine();
                    break;
                case RuntimeMode.Test:
                    runtime.UseTestHost();
                    runtime.ApplyCommandLine( string.Empty );
                    break;
            }
        }

        /// <summary>
        ///     Initialises this instance.
        /// </summary>
        /// <returns>
        /// </returns>
        [UsedImplicitly]
        public static TService Initialise() {
            HostFactory.Run( ConfigureRuntime );
            return Instance;
        }


        private static void ConfigureCustomRuntime( HostConfigurator runtime ) {


            // require local system privileges
            switch ( Options.AccountType ) {

                case ServiceAccountType.LocalService:
                    runtime.RunAsLocalService();
                    break;
                case ServiceAccountType.LocalSystem:
                    runtime.RunAsLocalSystem();
                    break;
                case ServiceAccountType.NetworkService:
                    runtime.RunAsNetworkService();
                    break;
                case ServiceAccountType.Custom:
                    // TODO: Add password to config (crypto?)
                    runtime.RunAs( Options.CustomAccount, Options.CustomAccount );
                    break;
            }


            // Pull options from our configuration instead
            runtime.SetDisplayName( Options.Name );
            runtime.SetServiceName( Options.SystemName );
            runtime.SetDescription( Options.Description );

            runtime.EnableServiceRecovery( RestartService );
        }

        private static void RestartService( ServiceRecoveryConfigurator configurator )
            => configurator.RestartService( Options.MaximumFailures );

        internal static void StartInstanceService() {
            if ( _initialised )
                return;

            Instance = new TService();
            _initialised = true;
        }

        internal static void StopInstanceService() {
            if ( _stopped )
                return;

            Instance.Dispose();
            Instance = null;
            _stopped = true;
            _initialised = false;
        }

        protected static void Say( string message, params object[] rest ) =>
            LogTo.Information( $"{{servicenameXYZ}} {message}", $"< {Options<ServiceOptions>.Current.Name}>", rest );

        protected static void Yell( string message, params object[] rest ) =>
            LogTo.Warning( $"{{servicenameXYZ}} {message}", $"< {Options<ServiceOptions>.Current.Name}>", rest );
    }
}