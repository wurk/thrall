﻿using System;
using NUnit.Framework;
using Topshelf;
using Topshelf.Logging;
using Wurk.Base;
using Wurk.Thrall.Options;

namespace Wurk.Thrall.Tests {

    /// <summary>
    /// </summary>
    [TestFixture]
    public class ThrallTests {

        /// <summary>
        ///     Sets up this instance.
        /// </summary>
        [OneTimeSetUp]
        public void Setup() =>
            Options<ServiceOptions>.Current = new ServiceOptions {
                Name = nameof( TestThrall ),
                AccountType = ServiceAccountType.LocalSystem,
                Description = "Testing the Thrall service library",
                SystemName = nameof( TestThrall )
            };


        [Test]
        public void TestThrallRestart() {
            try {
                using ( var startRaised = TestHelpers.AwaitStartEvent() ) {
                    using ( TestThrall.Initialise() ) {
                        DefaultServiceControl<TestThrall>.Restart();
                        Assert.IsTrue( startRaised?.WaitOne( 2500 ) );
                    }
                }
            } catch ( Exception ) {
                HostLogger.Shutdown();
                throw;
            }

        }

        /// <summary>
        ///     Tests the thrall starts.
        /// </summary>
        [Test]
        public void TestThrallStarts() {
            try {
                using ( var startRaised = TestHelpers.AwaitStartEvent() ) {
                    using ( TestThrall.Initialise() ) {
                        Assert.IsTrue( startRaised?.WaitOne( 1000 ) );
                    }
                }

                DefaultServiceControl<TestThrall>.Stop();
            } catch ( Exception ) {
                HostLogger.Shutdown();
                throw;
            }
        }

        /// <summary>
        /// Tests the thrall stops.
        /// </summary>
        [Test]
        public void TestThrallStops() {
            try {
                using ( var stopRaised = TestHelpers.AwaitStopEvent() ) {
                    using ( TestThrall.Initialise() ) {
                        DefaultServiceControl<TestThrall>.Stop();
                        Assert.IsTrue( stopRaised?.WaitOne( 2500 ) );
                    }

                }
            } catch ( Exception ) {
                HostLogger.Shutdown();
                throw;
            }
        }
    }
}