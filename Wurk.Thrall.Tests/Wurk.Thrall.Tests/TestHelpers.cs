﻿using System;
using System.Diagnostics.Contracts;
using System.Threading;

namespace Wurk.Thrall.Tests {
    public static class TestHelpers {

        internal static ManualResetEvent AwaitStartEvent() {
            Contract.Ensures( Contract.Result<ManualResetEvent>() != null );
            var @event = new ManualResetEvent( false );
            DefaultServiceControl<TestThrall>.ServiceStarted += HandleEvent( @event );
            return @event;
        }


        /// <summary>
        ///     Awaits the stop event.
        /// </summary>
        /// <returns>
        /// </returns>
        internal static ManualResetEvent AwaitStopEvent() {
            Contract.Ensures( Contract.Result<ManualResetEvent>() != null );
            var @event = new ManualResetEvent( false );
            DefaultServiceControl<TestThrall>.ServiceStopped += HandleEvent( @event );
            return @event;
        }

        private static EventHandler HandleEvent( ManualResetEvent @event ) {
            return ( o, e ) => @event?.Set();
        }
    }
}